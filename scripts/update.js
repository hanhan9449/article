const path = require("path");
const fs = require("fs");
const fse = require("fs-extra");
const hash = require("object-hash");
const publicPath = path.resolve(__dirname, "..", "public");
const markdownPath = path.resolve(__dirname, "..", "markdown");

let markdownDir = fs.readdirSync(markdownPath);

function checkPublicPath() {
  if (fs.existsSync(publicPath)) {
    fse.removeSync(publicPath);
  } else {
    fs.mkdirSync(publicPath);
  }
}

function main() {
  checkPublicPath();
  let ret = [];
  let indexHtml = {
    header: `<!DOCTYPE html>
<html lang="zh-hans">
<head>
  <meta charset="UTF-8">
  <title>文章列表</title>
</head>
<style>
    a {
        color: #101010;
        text-decoration: none;
    }
    li {
        line-height: 2;
    }
    a:hover {
        text-decoration: underline;
    }
</style>
<body>
<h1>文章列表</h1>
<ol>
`,
    footer: `
</ol>
</body>
</html>
`,
    body: ``,
  };

  for (const currentFileName of markdownDir) {
    let currentFilePath = path.resolve(markdownPath, currentFileName);
    let excludeFileList = ["index.md", "assets"];
    if (excludeFileList.includes(currentFileName)) {
      fse.copySync(currentFilePath, path.join(publicPath, currentFileName));
      continue;
    }
    const isDir = fs.statSync(currentFilePath).isDirectory();
    if (isDir) {
      let kindOfArticle = { typeName: currentFileName, items: [] };
      for (const nextFileName of fs.readdirSync(currentFilePath)) {
        const currFilePath = path.join(currentFilePath, nextFileName);
        let hashFileName = hash(path.join(currentFileName, nextFileName)) + ".md";
        let item = {
          name: nextFileName.replace(".md", ""),
          fileName: hashFileName,
        };
        indexHtml.body += `<li><a href="${item.fileName}">${item.name}</a></li>\n`;
        fse.copySync(currFilePath, path.join(publicPath, hashFileName));
        kindOfArticle.items.push(item);
      }
      ret.push(kindOfArticle);
    }
  }
  fs.writeFileSync(path.resolve(publicPath, "index.html"), indexHtml.header + indexHtml.body + indexHtml.footer);
  fs.writeFileSync(path.resolve(publicPath, "route.json"), JSON.stringify(ret, null, 2));
  console.log("更新route.json完成✅ ");
}

main();
