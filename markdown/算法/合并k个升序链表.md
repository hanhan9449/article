# 23. 合并 K 个升序链表

> https://leetcode-cn.com/problems/merge-k-sorted-lists/

```javascript
/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 */
var mergeKLists = function (lists) {
  if (lists.length === 0) {
    return null;
  }
  while (lists.length > 1) {
    let first = lists.shift();
    let second = lists.shift();
    lists.push(mergeTwoNodeList(first, second));
  }
  return lists[0];
};

function mergeTwoNodeList(first, second) {
  let dummy = new ListNode();
  let curr = dummy;
  while (first && second) {
    if (first.val > second.val) {
      curr.next = second;
      second = second.next;
    } else {
      curr.next = first;
      first = first.next;
    }
    curr = curr.next;
  }
  first && (curr.next = first);
  second && (curr.next = second);
  return dummy.next;
}
```
