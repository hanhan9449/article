# 235. 二叉搜索树的最近公共祖先

> https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/

```javascript
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */
var lowestCommonAncestor = function (root, p, q) {
  const vp = p.val;
  const vq = q.val;
  while (true) {
    if (root.val <= vp && root.val >= vq) {
      return root;
    }
    if (root.val >= vp && root.val <= vq) {
      return root;
    }
    if (root.val <= vq && root.val <= vp) {
      root = root.right;
    } else {
      root = root.left;
    }
  }
};
```
