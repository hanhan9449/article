# 开始学习golang

## 目的

记录一下自己学习golang的过程。

## 背景

想拓宽自己的技术栈，所以学习并记录学习golang的过程。

## 正文

### 如何安装环境

对于opensuse，直接通过`sudo zypper install go`即可安装golang的环境。

对于开发ide，我采用的是intellji家的goland。

### 如何学习

通过阅读相关材料进行学习。

1. [GO语言圣经](https://books.studygolang.com/gopl-zh/)——用来了解golang。


## 学习日历

- 2021/11/13 学习GO语言圣经中入门部分