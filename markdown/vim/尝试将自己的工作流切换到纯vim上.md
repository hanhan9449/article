# 尝试将自己的工作流切换到纯vim上

## 目的

记录自己配置vim，并将自己工作流切换到vim上的步骤。

## 背景

由于最近使用设备都为linux、macos系统，然后各个平台都有自己的编辑器，想通过vim来解决不同应用之间的切换上下文，因此决定将自己工作流切换到vim上。

## 正文

<!-- TODO -->
待补充......

### 安装gvim

在opensuse上，通过`sudo zypper install gvim`进行安装。

### 安装插件管理工具`vim-plug`

```shell
# 国内环境可能需要proxy来执行
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

### 安装常用插件

由于我在webstorm中日常开发也是使用的vim方案，所以我首先将自己的常用插件迁移过来，随后加一些文件管理、git相关的插件。

下面贴一下我的常用插件：

```
set nocompatible
set magic
call plug#begin()
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-commentary'
Plug 'vim-scripts/argtextobj.vim'
Plug 'kana/vim-textobj-entire'
Plug 'machakann/vim-highlightedyank'
Plug 'michaeljsmith/vim-indent-object'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
call plug#end()
```

## 时间线

- 2021/11/13 开始规划方案，准备实施。